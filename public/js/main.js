



$(document).ready(function(){
    $(".select-option").hide();
    $("#productType").change(function(){
        $(this).find("option:selected").each(function(){
            
            var optionValue = $(this).attr("value");
            if(optionValue){
                $(".select-option").not("#" + optionValue).hide();
                $("#" + optionValue).show();
            } else{
                $(".select-option").hide();
            }
        });
    }).change();

    $(".DVD").text("")
});

