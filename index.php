<?php
include_once "config/database.php";
include_once "controller/products-controller.php";
include_once "controller/product-types-controller.php";

// get database connection
$database = new Database();
$db = $database->getConnection();
$product = new ProductsController($db);
$type = new  ProductTypesController($db);


if (isset($_POST['delete_product'])) {
    $product->deleteProduct($_POST['deletedProd']);
}

include_once "header.php";

?>
<div class="site-wrapper-reveal site-wrapper-padding-top">
    <div id="product-list-wrap" class="section-space--ptb_80">
        <div class="container">
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" class="delete-product">
                <div class="row border-bottom border-dark">
                    <div class="col-lg-6">
                        <div class="section-title-wrap section-space--mb_20">
                            <h4 class="heading text-left">Product List
                            </h4>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="section-title-wrap section-space--mb_20">
                            <div class="row">
                                <div class="col-lg-7 text-right">
                                    <a href="add_product.php" class="btn btn-success text-uppercase">ADD</a>
                                </div>
                                <div class="col-lg-5">
                                    <button id="delete-product-btn" class="btn btn-danger text-uppercase" name="delete_product">MASS DELETE</button>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="row section-space--ptb_80">
                    <?php foreach ($product->getAllProduct() as $item) { ?>
                        <div class="col-lg-3 mb-20">
                            <div class="card shadow bg-white rounded">
                                <div class="card-header">
                                    <input type="checkbox" class="delete-checkbox form-check-input text-left" id="delete-checkbox" name="deletedProd[]" value="<?php echo $item->id ?>">
                                    <h5 class="card-title text-center"><?php echo $item->sku ?></h5>
                                </div>
                                <div class="card-body">

                                    <p class="card-text text-center" style="font-size: 20px;"><?php echo $item->name ?></p>
                                    <p class="card-text text-center"><?php echo $item->price ?> $</p>
                                    <p class="card-text text-center">
                                        <?php
                                        $prod_type = $type->getTypeNameById($item->productType);
                                        echo $product->getCompareType(new $prod_type->productId, $item->size, $item->weight, $item->width, $item->height, $item->length);
                                        ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </form>

        </div>
    </div>

</div>
<?php
include_once "footer.php";
?>