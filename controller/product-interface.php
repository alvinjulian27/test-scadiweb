<?php
    interface ProducTypeInt
    {
        public function getProductType($size, $weight, $width, $height, $length);
    }
