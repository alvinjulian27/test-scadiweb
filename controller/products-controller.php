

<?php
include_once "model/product.php";
include_once 'product-interface.php';
include_once 'product-type-compare.php';

class ProductsController
{
    private $conn;
    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function getInputData($params, $type)
    {
        if ($this->validateData($params, $type)) {
            $size = $this->isDataEmpty($params['size']);
            $weight = $this->isDataEmpty($params['weight']);
            $width = $this->isDataEmpty($params['width']);
            $height = $this->isDataEmpty($params['height']);
            $length = $this->isDataEmpty($params['length']);
            $product = new Product($params['sku'], $params['name'], $params['price'], $type, $size, $weight, $width, $height, $length);

            return $this->addDataProduct($product);
        }
        return false;
    }

    public function validateData($params, $type)
    {
        if (isset($params['sku']) && isset($params['name']) && isset($params['price']) && isset($type)) {
            return true;
        }

        return false;
    }

    public function isDataEmpty($data)
    {
        if (isset($data)) {
            return 0;
        }
        return $data;
    }

    public function addDataProduct(Product $product)
    {

        $query = "INSERT INTO product_list (sku, name, price, productType, size, weight, width, height, length) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(1, $product->sku);
        $stmt->bindParam(2, $product->name);
        $stmt->bindParam(3, $product->price);
        $stmt->bindParam(4, $product->productType);
        $stmt->bindParam(5, $product->size);
        $stmt->bindParam(6, $product->weight);
        $stmt->bindParam(7, $product->width);
        $stmt->bindParam(8, $product->height);
        $stmt->bindParam(9, $product->length);
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function getAllProduct()
    {
        $stmt = $this->conn->prepare('SELECT * FROM product_list');
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function deleteProduct($idProd)
    {
        for ($i = 0; $i < count($idProd); $i++) {
            $stmt = $this->conn->prepare('DELETE FROM product_list WHERE id= :id');
            $stmt->bindParam('id', $idProd[$i]);
            $stmt->execute();
        }
    }

    public function getCompareType(ProducTypeInt $type, $size, $weight, $width, $height, $length)
    {
        return $type->getProductType($size, $weight, $width, $height, $length);
    }
}
