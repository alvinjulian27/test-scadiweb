<?php
    class DVD implements ProducTypeInt
    {
        public function getProductType($size, $weight, $width, $height, $length)
        {
            return "Size: " . $size . " MB";
        }
    }
    
    class Book implements ProducTypeInt
    {
        public function getProductType($size, $weight, $width, $height, $length)
        {
            return "Weight: " . $weight . "KG";
        }
    }
    
    class Furniture implements ProducTypeInt
    {
        public function getProductType($size, $weight, $width, $height, $length)
        {
            return "Dimension: " . $width . "x" . $height . "x" . $length . " CM";
        }
    }
