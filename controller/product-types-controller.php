<?php
class ProductTypesController
{
    private $conn;
    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function getAllProductType()
    {

        $stmt = $this->conn->prepare('SELECT * FROM product_type');
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }
    public function searchIdProductType($data)
    {
        $stmt = $this->conn->prepare('SELECT id FROM product_type WHERE productId="' . $data . '"');
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ)[0];
    }

    public function getTypeNameById($id)
    {
        $stmt = $this->conn->prepare('SELECT productId FROM product_type WHERE id=?');
        $stmt->bindParam(1, $id);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_OBJ)[0];
    }
}
