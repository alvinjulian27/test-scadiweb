<?php

Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test ScadiWeb</title>
    <link rel="stylesheet" href="public/css/vendor/vendor.min.css">
    <!-- <link rel="stylesheet" href="css/plugins/plugins.min.css"> -->
    <link rel="stylesheet" href="public/css/style.css">
</head>