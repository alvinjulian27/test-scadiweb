<?php
class Product
{

    private $conn;

    public $id;
    public $sku;
    public $name;
    public $price;
    public $productType;
    public $size;
    public $weight;
    public $width;
    public $height;
    public $length;

    public function __construct($sku, $name, $price, $type, $size, $weight, $width, $height, $length)
    {
        $this->setSku($sku);
        $this->setName($name);
        $this->setPrice($price);
        $this->setProductType($type);
        $this->setSize($size);
        $this->setWeight($weight);
        $this->setWidth($width);
        $this->setHeight($height);
        $this->setLength($length);
    }

    public function setSku($sku)
    {
        $this->sku = $sku;
    }
    public function setName($name)
    {
        $this->name = $name;
    }
    public function setPrice($price)
    {
        $this->price = $price;
    }
    public function setProductType($productType)
    {
        $this->productType = $productType;
    }
    public function setSize($size)
    {
        $this->size = $size;
    }
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }
    public function setWidth($width)
    {
        $this->width = $width;
    }
    public function setHeight($height)
    {
        $this->height = $height;
    }
    public function setLength($length)
    {
        $this->length = $length;
    }

    public function getSku()
    {
        return $this->sku;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getPrice()
    {
        return $this->price;
    }
    public function getProductType()
    {
        return $this->productType;
    }
    public function getSize()
    {
        return $this->size;
    }
    public function getWeight()
    {
        return $this->weight;
    }
    public function getWidth()
    {
        return $this->width;
    }
    public function getHeight()
    {
        return $this->height;
    }
    public function getLength()
    {
        return $this->length;
    }
}
