<?php
include_once "config/database.php";
include_once "controller/products-controller.php";
include_once "controller/product-types-controller.php";

// get database connection
$database = new Database();
$db = $database->getConnection();

// pass connection to objects
$product = new ProductsController($db);
$type = new ProductTypesController($db);
$stmt = $type->getAllProductType();
$errorMsg = "";

if (isset($_POST['submit_form'])) {
    $type_prod = $type->searchIdProductType($_POST['productType']);
    if ($product->getInputData($_POST, $type_prod->id) == true) {
        header('Location: http://localhost/test/index.php');
    } else {
        $errorMsg = "Please, submit required data";
    }
}

include_once "header.php";
?>

<div class="site-wrapper-reveal site-wrapper-padding-top">
    <div id="product-add-wrap" class="section-space--ptb_80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" id="product_form">
                        <div class="row border-bottom border-dark mb-40">
                            <div class="col-lg-6">
                                <div class="section-title-wrap section-space--mb_20">
                                    <h4 class="heading text-left">Product List
                                    </h4>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="section-title-wrap section-space--mb_20">
                                    <div class="row">
                                        <div class="col-lg-8 text-right">
                                            <button type="submit" class="btn btn-primary" name="submit_form">Save</button>
                                        </div>
                                        <div class="col-lg-4">
                                            <a href="index.php" class="btn btn-warning">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">SKU</label>
                            <div class="col-sm-10">
                                <input type="text" id="sku" name="sku" class='form-control' />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" id="name" name="name" class='form-control' />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Price</label>
                            <div class="col-sm-10">
                                <input type="text" id="price" name="price" class='form-control' />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Type Switcher</label>
                            <div class="col-sm-10">
                                <?php
                                echo "<select class='form-control custom-select' name='productType' id='productType'>";
                                echo "<option>Select category...</option>";

                                foreach ($stmt as $item) {
                                    echo "<option value='{$item->productId}'>{$item->name}</option>";
                                }

                                echo "</select>";

                                ?>
                            </div>
                        </div>
                        <div id="DVD" class="select-option">
                            <div class="form-group row">
                                <label for="size" class="col-sm-2 col-form-label">Size (MB)</label>
                                <div class="col-sm-10">
                                    <input type="number" id="size" name='size' class='form-control' />
                                </div>
                                <h6 class="mt-20">Please, provide size in MB</h6>
                            </div>
                        </div>
                        <div id="Book" class="select-option">
                            <div class="form-group row">
                                <label for="weight" class="col-sm-2 col-form-label">Weight (KG)</label>
                                <div class="col-sm-10">
                                    <input type="number" id="weight" name="weight" class='form-control' />
                                </div>
                                <h6 class="mt-20">Please, provide weight in KG</h6>
                            </div>
                        </div>
                        <div id="Furniture" class="select-option">
                            <div class="form-group row">
                                <label for="height" class="col-sm-2 col-form-label">Height (CM)</label>
                                <div class="col-sm-10">
                                    <input type="number" id="height" name="height" class='form-control' />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="width" class="col-sm-2 col-form-label">Width (CM)</label>
                                <div class="col-sm-10">
                                    <input type="number" id="width" name="width" class='form-control' />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="length" class="col-sm-2 col-form-label">Length (CM)</label>
                                <div class="col-sm-10">
                                    <input type="number" id="length" name="length" class='form-control' />
                                </div>
                            </div>
                            <h6 class="mt-20">Please, provide dimensions in CM</h6>
                        </div>
                        <?php if (!empty($errorMsg)) { ?>
                            <div class="alert alert-danger">
                                <h5><?php echo $errorMsg ?></h5>
                            </div>
                        <?php } ?>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
<?php
include_once "footer.php";
?>